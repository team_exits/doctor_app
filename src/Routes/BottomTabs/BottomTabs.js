import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeScreen from '../../Views/HomeScreen';
import {View, Text, TouchableOpacity} from 'react-native';
import HomeStack from '../Stacks/HomeStack/HomeStack';
import AppointmentStack from '../Stacks/AppointmentStack/AppointmentStack';
import ListStack from '../Stacks/ListStack/ListStack';
import AccountStack from '../Stacks/AccountStack/AccountStack';
import Home from 'react-native-vector-icons/Entypo';
const Tab = createBottomTabNavigator();

function MyTabBar({state, descriptors, navigation}) {
  return (
    <View style={{flexDirection: 'row'}}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{flex: 1}}>
            <Text style={{color: isFocused ? '#673ab7' : '#222'}}>{label}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const BottomTabs = () => {
  return (
    <Tab.Navigator
      initialRouteName="HomeStack"
      tabBarOptions={{
        keyboardHidesTabBar: true,
        activeTintColor: '#f06b32',
        inactiveTintColor: '#cbcbcb',
        showLabel: true,
        style: {height: 60},
        tabStyle: {marginTop: 5},
        labelStyle: {fontSize: 13, paddingBottom: 5},
      }}>
      <Tab.Screen
        name="HomeStack"
        component={HomeStack}
        options={({route}) => ({
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <Home name="home" size={size} color={color} />
          ),
        })}
      />
      <Tab.Screen
        name="AppointmentStack"
        component={AppointmentStack}
        options={({route}) => ({
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <Home name="home" size={size} color={color} />
          ),
        })}
      />
      <Tab.Screen
        name="ListStack"
        component={ListStack}
        options={({route}) => ({
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <Home name="home" size={size} color={color} />
          ),
        })}
      />
      <Tab.Screen
        name="AccountStack"
        component={AccountStack}
        options={({route}) => ({
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <Home name="home" size={size} color={color} />
          ),
        })}
      />
      {/* <Tab.Screen name="HomeScreen" component={HomeScreen} />
      <Tab.Screen name="Appointment" component={Appointment} /> */}
    </Tab.Navigator>
  );
};

export default BottomTabs;
