import React from 'react';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Appointment from '../../../Views/BottomTabScreen/Appointment';
const Stack = createNativeStackNavigator();
export default function AppointmentStack() {
  return (
    // define here all Home tabs route here
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Appointment" component={Appointment} />
    </Stack.Navigator>
  );
}
