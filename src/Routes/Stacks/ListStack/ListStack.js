import React from 'react';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Listing from '../../../Views/BottomTabScreen/Listing';
const Stack = createNativeStackNavigator();
export default function ListStack() {
  return (
    // define here all Home tabs route here
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Listing" component={Listing} />
    </Stack.Navigator>
  );
}
