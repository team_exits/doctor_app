import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
const Register = ({navigation}) => {
  return (
    <>
      <ScrollView showsVerticalScrollIndicator>
        <ImageBackground
          source={require('../../Assets/backgraound.png')}
          style={{
            flex: 1,
            resizeMode: 'cover',
            flexDirection: 'column',
            alignItems: 'center',
          }}>
          <View style={{alignSelf: 'center'}}>
            <View
              style={{
                alignItems: 'center',
              }}>
              <Image
                source={require('../../Assets/logo.png')}
                style={{
                  flexDirection: 'row',
                  marginTop: "9%",
                }}
              />
              <Text
                style={{
                  marginTop: '3%',
                  color: 'white',
                  flexDirection: 'column',
                  alignItems: 'center',
                  fontWeight: '500',
                }}>
                lorem ipsum dolor sit amet
              </Text>
              <Text
                style={{
                  color: 'white',
                  alignItems: 'center',
                  flexDirection: 'column',
                  fontWeight: '500',
                }}>
                lorem ipsum dolor sit amet lorem ipsum dolor
              </Text>
            </View>

            <View
              style={{
                marginLeft: '15%',
                marginTop: '15%',
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontSize: rf(3.5),
                  fontWeight: 'bold',
                  color: '#000',
                  alignSelf: 'center',
                }}>
                Verify Yourself
              </Text>
            </View>

            <View
              style={{
                borderColor: 'grey',
                borderWidth: 1,
                padding: 5,
                width: rw(80),
                height: rh(7),
                borderRadius: 45,
                borderColor: '#1D2226',
                marginTop: '3%',
              }}>
              <TextInput
                style={{marginLeft: '5%'}}
                placeholder="Name"></TextInput>
            </View>

            <View
              style={{
                borderColor: 'grey',
                borderWidth: 1,
                padding: 5,
                paddingLeft: 10,
                width: rw(80),
                height: rh(7),
                borderRadius: 45,
                marginTop: '5%',
                flexDirection: 'row',
                borderColor: '#1D2226',
                //justifyContent: 'space-around',
              }}>
              <TextInput placeholder="Mobile Number" />
            </View>

            <View
              style={{
                borderColor: 'grey',
                borderWidth: 1,
                padding: 5,
                width: rw(80),
                height: rh(7),
                borderRadius: 45,
                marginTop: '5%',
                borderColor: '#1D2226',
              }}>
              <TextInput style={{marginLeft: '5%'}} placeholder="Email" />
            </View>

            <View
              style={{
                borderColor: 'grey',
                borderWidth: 1,
                padding: 5,
                width: rw(80),
                height: rh(7),
                borderRadius: 45,
                marginTop: '5%',
                borderColor: '#1D2226',
              }}>
              <TextInput
                style={{marginLeft: '5%'}}
                placeholder="Referral Code (optional)"
              />
            </View>

            <View style={{marginTop: '5%'}}>
              <Text>You receive an SMS to verify your phone identity</Text>
            </View>

            <View>
              <TouchableOpacity
                style={{
                  marginVertical: '2%',
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: '#E93D43',
                  borderWidth: 1,
                  padding: 5,
                  width: rw(80),
                  height: rh(7),
                  borderRadius: 45,
                  marginTop: '5%',
                  borderColor: '#E93D43',
                }} onPress={() => navigation.navigate('Login')}>
                <Text
                  style={{color: 'white', fontSize: 18, fontWeight: 'bold'}}>
                  Continue
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                marginTop: '7%',
                // padding: 20,
                display: 'flex',
                justifyContent: 'center',
                align: 'center',
              }}>
              <Text style={{color: 'white', alignSelf: 'center'}}>
                By signing up, you are agree with our{'\n'}
                <Text style={{textDecorationLine: 'underline'}}>
                  Terms Of Service <Text> </Text>
                </Text>
                <Text>&</Text>
                <Text style={{textDecorationLine: 'underline'}}>
                  <Text> </Text> Privacy Policy
                </Text>
              </Text>
            </View>
          </View>
        </ImageBackground>
      </ScrollView>
    </>
  );
};

export default Register;
