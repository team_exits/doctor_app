import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
import Mail from "react-native-vector-icons/AntDesign";
import Lock from "react-native-vector-icons/Feather";

const Login = ({navigation}) => {
  return (
    <>
      <>
        {/* <ImageBackground
          style={{
            flex: 1,
            resizeMode: 'cover',
            flexDirection: 'column',

            alignItems: 'center',
          }}
          source={require('../../Assets/fullscreen.png')}>
          <Image
            source={require('../../Assets/logo.png')}
            style={{height: '12%', width: '19%', marginTop: '5%'}}
          />
          <Text style={{textAlign: 'center', color: '#FFF'}}>
            lorem ipsum dolor sit amet{'\n'}
            lorem ipsum dolor sit amet lorem ipsum dolor{' '}
          </Text>
          <View
            style={{
              // justifyContent: 'center',
              // alignItems: 'center',
              flexDirection: 'column',
              marginTop: '20%',
            }}>
            <Text style={{fontSize: rf(3), color: '#000'}}>Login</Text>
            <Text>Please login to your account.</Text>
            <View
              style={{
                borderColor: 'grey',
                borderWidth: 1,
                padding: 3,
                width: rw(80),
                height: rh(7),
                borderRadius: 45,
                marginTop: '5%',
              }}>
              <TextInput placeholder="Phone/Email Address"></TextInput>
            </View>
            <View
              style={{
                borderColor: 'grey',
                borderWidth: 1,
                padding: 3,
                paddingLeft: 10,
                width: rw(80),
                height: rh(7),
                borderRadius: 45,
                marginTop: '5%',
                flexDirection: 'row',
                //justifyContent: 'space-around',
              }}>
              <TextInput placeholder="Password" />
            </View>
          </View>

          <View style={{marginTop: '5%'}}>
            <TouchableOpacity
              style={{textAlign: 'center'}}
              onPress={() => navigation.navigate('Register')}>
              <Text> Don’t have an account ? Create new now!</Text>
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              style={{
                borderColor: 'grey',
                borderWidth: 1,
                padding: 5,
                width: rw(80),
                height: rh(7),
                borderRadius: 45,
                marginTop: '5%',
                marginLeft: '4%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#e92329',
              }}
              onPress={() => navigation.navigate('HomeScreen')}>
              <Text style={{color: 'white', fontSize: 18}}>Login</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground> */}
        <ImageBackground
          source={require('../../Assets/backgraound.png')}
          style={{
            flex: 1,
            resizeMode: 'cover',
            flexDirection: 'column',
            alignItems: 'center',
          }}>
          <View style={{alignSelf: 'center'}}>
            <View
              style={{
                alignItems: 'center',
              }}>
              <Image
                source={require('../../Assets/logo.png')}
                style={{
                  flexDirection: 'row',
                  marginTop: '9%',
                }}
              />
              <Text
                style={{
                  marginTop: '1%',
                  color: 'white',
                  flexDirection: 'column',
                  alignItems: 'center',
                  fontWeight: '500',
                }}>
                lorem ipsum dolor sit amet
              </Text>
              <Text
                style={{
                  color: 'white',
                  alignItems: 'center',
                  flexDirection: 'column',
                  fontWeight: '500',
                }}>
                lorem ipsum dolor sit amet lorem ipsum dolor
              </Text>
            </View>

            <View
              style={{
                // justifyContent: 'center',
                // alignItems: 'center',
                flexDirection: 'column',
                marginTop: '20%',
              }}>
              <Text
                style={{fontSize: rf(3), color: '#1D2226', fontWeight: '900'}}>
                Login
              </Text>
              <Text style={{fontSize: rf(2), color: '#707070'}}>
                Please login to your account.
              </Text>
              <View
                style={{
                  borderColor: 'grey',
                  borderWidth: 1,
                  padding:8,
                  width: rw(80),
                  height: rh(7.5),
                  borderRadius: 45,
                  marginTop: '5%',
                  justifyContent:'space-between',
                  flexDirection:'row',
                }}>
                <TextInput placeholder="Phone/Email Address" style={{fontSize:rf(2)}}></TextInput>
                <Mail name="mail" size={25} style={{alignItems:'center',alignContent:'center'}}/>
              </View> 
              <View
                style={{
                  borderColor: 'grey',
                  borderWidth: 1,
                  padding:8,
                  paddingLeft: 10,
                  width: rw(80),
                  height: rh(7.5),
                  borderRadius: 45,
                  marginTop: '5%',
                  // flexDirection: 'row',
                  justifyContent:'space-between',
                  flexDirection:'row',
                  //justifyContent: 'space-around',
                }}>
                <TextInput placeholder="Password" />
                <Lock name="lock" size={25} style={{alignItems:'center',alignContent:'center'}}/>
              </View>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <TouchableOpacity
                  style={{
                    borderColor: '#1A1A1A',
                    borderWidth: 0.5,
                    padding: 5,
                    width: rw(80),
                    height: rh(7),
                    borderRadius: 50,
                    marginTop: '5%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#E93D43',
                  }}
                  onPress={() => navigation.navigate('HomeScreen')}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: rf(3),
                      fontWeight: '900',
                    }}>
                    Login
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View
              style={{
                marginTop: '20%',
                justifyContent: 'center',
                flex: 1,
                textAlign: 'center',
              }}>
              <Text style={{textAlign: 'center'}}>
                {' '}
                Don’t have an account ?{' '}
                <Text
                  onPress={() => navigation.navigate('Register')}
                  style={{color: '#e93d43'}}>
                  Create new now!
                </Text>
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'flex-end',
                flex: 1,
              }}>
              <Text
                style={{
                  color: 'white',
                  alignSelf: 'center',
                  fontSize: rf(1.6),
                }}>
                By signing up, you are agree with our{'\n'}
                <Text style={{textDecorationLine: 'underline'}}>
                  Terms Of Service <Text> </Text>
                </Text>
                <Text>&</Text>
                <Text style={{textDecorationLine: 'underline'}}>
                  <Text> </Text> Privacy Policy
                </Text>
              </Text>
            </View>
          </View>
        </ImageBackground>
      </>
    </>
  );
};

export default Login;
