import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
import TimeEve from '../../Components/timeEve';
import CalendarStrip from 'react-native-calendar-strip';

export default function Appointment() {
  return (
    <>
      <View style={styles.container}>
        <ImageBackground
          style={styles.stretch}
          imageStyle={{borderBottomLeftRadius: 40, borderBottomRightRadius: 40}}
          source={require('../../Assets/Rectangle32.png')}>
          <View style={styles.logoView}>
            <Image
              style={styles.logo}
              source={require('../../Assets/logo.png')}
            />
          </View>
          <View style={{display: 'flex', justifyContent: 'space-around'}}>
            <Text
              style={{
                fontSize: rf(3.5),
                color: 'white',
                paddingLeft: 25,
                fontWeight: 'bold',
              }}>
              Book An Appointment
            </Text>
          </View>
          <View style={styles.details}>
            <View style={styles.dpview}>
              <Image
                style={styles.dp}
                source={require('../../Assets/Rectangle27.png')}
              />
            </View>
            <View style={styles.names}>
              <Text
                style={{
                  fontSize: rf(2.5),
                  color: 'white',
                  paddingLeft: 20,
                  fontWeight: 'bold',
                }}>
                Dr. Martin Piller
              </Text>
              <Text style={{fontSize: rf(2), color: 'white', paddingLeft: 20}}>
                BDS certifid training hands on alpha
              </Text>
              <Text style={{fontSize: rf(2), color: 'white', paddingLeft: 20}}>
                Cardiologist
              </Text>
              <Text style={{fontSize: rf(2), color: 'white', paddingLeft: 20}}>
                sunwave Hospital Dlelhi
              </Text>
            </View>
          </View>
          <View style={styles.rate}>
            <Text style={{fontSize: rf(2), color: 'white', paddingLeft: 20}}>
              10 Year Exp - ₹ 200
            </Text>
          </View>

          {/* <View style={{ borderBottomWidth:1, width: rw(85), display: 'flex', justifyContent: 'center', alignItems: 'center'}}>

          </View> */}
        </ImageBackground>
        <View style={styles.components}>
          <CalendarStrip
            calendarHeaderStyle={styles.calendarHeaderStyle}
            style={{height: rh(10), width: rw(83)}}
          />
        </View>
        {/* <View style={styles.components}>
          <TimeMor />
        </View> */}
        <View style={styles.components}>
          <TimeEve />
        </View>
        <View></View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  stretch: {
    height: rh(40),
    width: rw(100),

    marginBottom: 30,
  },
  logoView: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: rh(14),
  },
  container: {
    // borderWidth: 1,
    backgroundColor: '#f5f5f5',
    height: rh(100),
  },
  logo: {
    height: rh(8),
    width: rw(15),
  },
  dp: {
    height: rh(12),
    width: rw(22),
    borderRadius: 2,
  },
  details: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',

    width: rw(85),
    margin: 10,
    padding: 10,
  },
  names: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  rate: {
    display: 'flex',

    width: rw(60),
    paddingLeft: 5,
  },
  components: {
    display: 'flex',

    justifyContent: 'center',
    alignItems: 'center',
  },
  calendarHeaderStyle: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
});
