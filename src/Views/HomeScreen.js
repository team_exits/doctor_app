import React from 'react';
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
const HomeScreen = ({navigation}) => {
  return (
    <SafeAreaView>
      <ScrollView>
        <View
          style={{
            flexDirection: 'column',
          }}>
          <View style={{width: '100%', height: '5%'}}>
            <Text>Icon</Text>
          </View>
          <Image source={require('../Assets/homehead.png')} />
          <View style={{margin: 10}}>
            <View>
              <Text>Trending Topics</Text>
              <View
                style={{
                  justifyContent: 'space-around',
                  flexDirection: 'row',
                }}>
                <View style={{flexDirection: 'column'}}>
                  <Image source={require('../Assets/Asthma.png')} />
                  <Text style={{color: '#E93D43', fontSize: rf(1.8)}}>
                    Asthma
                  </Text>
                </View>
                <View style={{flexDirection: 'column'}}>
                  <Image source={require('../Assets/Gingivitis.png')} />
                  <Text style={{color: '#E93D43', fontSize: rf(1.8)}}>
                    Gingivitis
                  </Text>
                </View>
                <View style={{flexDirection: 'column'}}>
                  <Image source={require('../Assets/Acne.png')} />
                  <Text style={{color: '#E93D43', fontSize: rf(1.8)}}>
                    Acne
                  </Text>
                </View>
                <View style={{flexDirection: 'column'}}>
                  <Image source={require('../Assets/Myopia.png')} />
                  <Text style={{color: '#E93D43', fontSize: rf(1.8)}}>
                    Myopia
                  </Text>
                </View>
                <View style={{flexDirection: 'column'}}>
                  <Image source={require('../Assets/Migrant.png')} />
                  <Text style={{color: '#E93D43', fontSize: rf(1.8)}}>
                    Migrant
                  </Text>
                </View>
              </View>
            </View>
            <View style={{marginTop: 5}}>
              <Text>Doctors</Text>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  marginTop: 5,
                }}>
                <View style={{flexDirection: 'column'}}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Appointment')}>
                    <Image source={require('../Assets/doctor1.png')} />
                    <Text style={{fontSize: rf(1.5), color: '#E93D43'}}>
                      Dr.Fred Mask
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'column'}}>
                  <Image source={require('../Assets/doctor2.png')} />
                  <Text style={{fontSize: rf(1.5), color: '#E93D43'}}>
                    Dr Stella Kane
                  </Text>
                </View>
                <View style={{flexDirection: 'column'}}>
                  <Image source={require('../Assets/doctor3.png')} />
                  <Text style={{fontSize: rf(1.5), color: '#E93D43'}}>
                    Dr Zac Wolff
                  </Text>
                </View>
                <View style={{flexDirection: 'column'}}>
                  <Image source={require('../Assets/doctor4.png')} />
                  <Text style={{fontSize: rf(1.5), color: '#E93D43'}}>
                    Dr Mac Jodiac
                  </Text>
                </View>
              </View>
            </View>
            <View style={{marginTop: 5}}>
              <Text>Specialists</Text>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  marginTop: 5,
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    height: rh(25),
                    width: rw(32),
                    alignItems: 'center',
                    borderRadius: 10,
                    justifyContent: 'space-between',
                    elevation: 2,
                    //backgroundColor: 'red',
                  }}>
                  <Image
                    source={require('../Assets/special1.png')}
                    style={{marginTop: 5}}
                  />
                  <Text style={{fontSize: rf(1.5), color: '#E93D43'}}>
                    Dr John Yoo
                  </Text>
                  <Text style={{fontSize: rf(1.3)}}>General Practitioner</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'column',
                    height: rh(25),
                    width: rw(32),
                    alignItems: 'center',
                    borderRadius: 10,
                    elevation: 2,
                    //backgroundColor: 'red',
                  }}>
                  <Image
                    source={require('../Assets/special2.png')}
                    style={{marginTop: 5}}
                  />
                  <Text style={{fontSize: rf(1.5), color: '#E93D43'}}>
                    Dr John Yoo
                  </Text>
                  <Text style={{fontSize: rf(1.3)}}>General Practitioner</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'column',
                    height: rh(25),
                    width: rw(32),
                    alignItems: 'center',
                    borderRadius: 10,
                    elevation: 2,
                    //backgroundColor: 'red',
                  }}>
                  <Image
                    source={require('../Assets/special3.png')}
                    style={{marginTop: 5}}
                  />
                  <Text style={{fontSize: rf(1.5), color: '#E93D43'}}>
                    Dr John Yoo
                  </Text>
                  <Text style={{fontSize: rf(1.3)}}>General Practitioner</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Image source={require('../Assets/homefoot.png')} />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;
